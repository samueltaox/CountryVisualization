import peasy.*;

class Base{
  float mainX;
  float mainY;
  float mainRad = 500;
  float secondRad = 200;
  String c1_low, c2_low, c3_low, c1_high, c2_high, c3_high, c1_mid, c2_mid, c3_mid;
  PeasyCam cam;
  
  Base(PApplet parent, PGraphics pg){
    cam = new PeasyCam(parent, pg, 250, 110, 0, 450);
    cam.setMinimumDistance(400);
    cam.setMaximumDistance(500);
  }
  
  void loadStrings(List<String> l, int currentDomain){
    int id;
    String currentValue;
    List<Float> choosedCountriesProp = new ArrayList<Float>();
    for(int i=0; i<l.size(); i++){
      id = getCountryId(l.get(i));
      currentValue = data[id][currentDomain];
      
      if(!currentValue.equals("")){
        choosedCountriesProp.add(Float.parseFloat(currentValue));
      }
      else{
        choosedCountriesProp.add(Float.parseFloat("-1"));
      }
      
      if(i > 8) break;
    }
    SortedMap<Float, String> map = new TreeMap<Float, String>();
    
    for(int i=0; i<choosedCountriesProp.size(); i++){
      map.put(choosedCountriesProp.get(i), l.get(i));
    }
    if(map.size() == 0) {
        c1_low = "";
        c1_mid = "";
        c1_high = "";
        c2_low = "";
        c2_mid = "";
        c2_high = "";
        c3_low = "";
        c3_mid = "";
        c3_high = "";
    }
    else if(map.size() == 1) c1_mid = map.get(map.firstKey());
    else if(map.size() == 2){
        c1_low = map.get(map.firstKey());
        c1_mid = "";
        c1_high = map.get(map.lastKey());
        c2_low = "";
        c2_mid = "";
        c2_high = "";
        c3_low = "";
        c3_mid = "";
        c3_high = "";
     }
     else if(map.size() == 3){
        c1_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_mid = map.get(map.firstKey());
        System.out.println(map.firstKey());
        map.remove(map.firstKey());
        c1_high = map.get(map.firstKey());
        System.out.println(map.firstKey());
        map.remove(map.firstKey());
        c2_low = "";
        c2_mid = "";
        c2_high = "";
        c3_low = "";
        c3_mid = "";
        c3_high = "";
      }
      else if(map.size() == 4){
        c1_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_low = "";
        c2_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_high = "";
        c3_low = "";
        c3_mid = "";
        c3_high = "";
      }
      else if(map.size() == 5){
        c1_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_low = "";
        c1_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_mid = "";
        c3_mid = "";
        c1_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_high = "";
      }
      else if(map.size() == 6){
        c1_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_low = "";
        c3_mid = "";
        c3_high = "";
      }
      else if(map.size() == 7){
        c1_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_low = "";
        c3_high = "";
      }
      else if(map.size() == 8){
        c1_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_mid = "";
        c3_high = map.get(map.firstKey());
        map.remove(map.firstKey());
      }
      else if(map.size() == 9){
        c1_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_low = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c2_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_mid = map.get(map.firstKey());
        map.remove(map.firstKey());
        c1_high = map.get(map.firstKey());
        map.remove(map.firstKey());   
        c2_high = map.get(map.firstKey());
        map.remove(map.firstKey());
        c3_high = map.get(map.firstKey());
        map.remove(map.firstKey());
      }
  }
  
  void draw(PGraphics pg, int pw, int ph){
    labelFont = loadFont("TimesNewRomanPSMT-32.vlw");
    pg.beginDraw();
    pg.textFont(labelFont);
    pg.background(255);
    pg.strokeWeight(4);
    pg.translate(250, 110, 0);
    pg.rotateX(PI/2);
    //pg.fill(255);
    //pg.ellipse(0, 0, mainRad, mainRad);
    pg.rotateX(-PI/2);
    pg.translate(0, 0, -mainRad/2);
    pg.translate(0, 0, -1);
    pg.rotateY(PI);
    pg.fill(0);
    pg.text("HIGH", -40, -60);
    pg.text(c1_high, -55, -20);
    pg.text(c2_high, -55, 15);
    pg.text(c3_high, -55, 50);
    pg.rotateY(-PI);
    pg.translate(0, 0, 1);
    pg.fill(255);
    pg.ellipse(0, 0, secondRad, secondRad);
    pg.translate(mainRad*sqrt(3)/4, 0, 3*mainRad/4);
    pg.translate(0, 0, 1);
    pg.rotateY(PI/3);
    pg.fill(0);
    pg.text("MID", -35, -60);
    pg.text(c1_mid, -55, -20);
    pg.text(c2_mid, -55, 15);
    pg.text(c3_mid, -55, 50);
    pg.translate(0, 0, -1);
    pg.fill(255);
    pg.ellipse(0, 0, secondRad, secondRad);
    pg.rotateY(-PI/3);
    pg.translate(-mainRad*sqrt(3)/2, 0, 0);
    pg.rotateY(-PI/3);
    pg.translate(0, 0, 1);
    pg.fill(0);
    pg.text("LOW", -35, -60);
    pg.text(c1_low, -55, -20);
    pg.text(c2_low, -55, 15);
    pg.text(c3_low, -55, 50);
    pg.translate(0, 0, -1);
    pg.fill(255);
    pg.ellipse(0, 0, secondRad, secondRad);
    pg.endDraw();
  }
}
