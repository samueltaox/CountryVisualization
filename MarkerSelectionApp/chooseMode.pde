static final int MODE_DEFAULT = 0;
static final int MODE_HORIZONTAL = 1;
static final int MODE_VERTICAL = 2;
static final int MODE_CYCLE = 3;
static final int MODE_FREECHOOSE = 4;
class chooseMode{
    float x = 100;
    float y = 800;
    float w = 150;
    float h = 80;
    int currentMode;
    boolean calcul = false;
    int lineX;
    int lineY;

    float radius;
    boolean moveC;
    boolean changeSize;
    float value;
    float meanValue;
    float num;

    //randomly pick up the current attribute
    int currentDomain = 8;

    List<String> choosedCountry;
    List<Marker> choosedMarker;
    boolean clicked = false;
    int maxC = 100;
    Boolean in = false;
    Boolean locked = false;
    PFont labelFont;
    chooseMode(){
        currentMode = 0;
        labelFont = loadFont("AccanthisADFStdNo2-BoldItalic-24.vlw");
        textFont(labelFont,32);
        lineX = sizeX/2;
        lineY = sizeY/2;
        choosedCountry = new LinkedList<String>();
        choosedMarker = new LinkedList<Marker>();
        choosedCountry.clear();
        choosedMarker.clear();

        radius = 30;
        moveC = false;
        changeSize = false;
    }
    void draw(){
        fill(255);
        rect(x,y,w,h);

        if(mouseX>x && mouseX <x+w && mouseY>y && mouseY <y+h){
            //println("The mouse is pressed and over the button");
            //fill(0);
            //do stuff
           // currentMode = (currentMode + 1) % 5;
           in = true;
        }
        else in = false;

        ShowText();
        ShowCurrentMode();
    }

    void ShowCurrentMode(){
        switch(currentMode){
            case MODE_DEFAULT:{
                //text("no mode", x, y + h/2);
                choosedCountry.clear();
                choosedMarker.clear();
                break;
            }
            case MODE_HORIZONTAL:{
                ModeHorizontal();
                choosedCountry.clear();
                choosedMarker.clear();
                break;
            }
            case MODE_VERTICAL:{
                choosedCountry.clear();
                choosedMarker.clear();
                ModeVertical();
                break;
            }
            case MODE_CYCLE:{
                ModeCycle();
                choosedCountry.clear();
                choosedMarker.clear();
                break;
            }
            case MODE_FREECHOOSE:{
                ModeFreeChoose();
                break;
            }
            default: break;
        }
    }
    void ModeHorizontal(){

        fill(0,0,0);
        if(mousePressed){
            if(mouseX > lineX - 2 && mouseX < lineX + 2){
                locked = true;
            }
        }
        dottedLine(lineX,0,lineX,sizeY,100);

        // get country in this line
        //if(locked == false){

        if(calcul == true){
           value = 0;
           num = 0;
            for (Marker marker : map.getMarkers()) {
                //println(marker.getProperty("name"));
                marker.setSelected(false);
            }

            String lastCountry = "";
            String currentCountry = "";
            for(int i = 0;i < sizeY;i += 10){
                Marker marker = map.getFirstHitMarker(lineX, i);
                if (marker != null) {
                    currentCountry = marker.getStringProperty("name");
                    if(currentCountry!= null){
                        if(currentCountry.equals(lastCountry)) continue;
                        else if(!currentCountry.equals("")){
                            marker.setSelected(true);
                            lastCountry = currentCountry;
                            int id = getCountryId(currentCountry);
                            if(id != -1){
                                String currentValue = data[id][currentDomain];

                                if(!currentValue.equals("")){

                                    value += Float.parseFloat(currentValue);
                                    num += 1;
                                }
                            }
                        }
                    }

                }
            }
            meanValue = value / num;
            calcul = false;
        }

        fill(0,0,0);
        text("mean value for chosen countries is " + new DecimalFormat("0.00").format(meanValue), 400, 900);
           text("total value for chosen countries is " + new DecimalFormat("0.00").format(value), 400, 930);
    }

    void ModeVertical(){
        fill(0,0,0);
        if(mousePressed){
            if(mouseY > lineY - 2 && mouseY < lineY + 2){
                locked = true;
            }
        }
        dottedLine(0,lineY,sizeX,lineY,100);

         if(calcul == true){
           value = 0;
           num = 0;
            for (Marker marker : map.getMarkers()) {
                //println(marker.getProperty("name"));
                marker.setSelected(false);
            }

            String lastCountry = "";
            String currentCountry = "";
            for(int i = 0;i < sizeX;i += 10){
                Marker marker = map.getFirstHitMarker(i, lineY);
                if (marker != null) {
                    currentCountry = marker.getStringProperty("name");
                    if(currentCountry!= null){
                        if(currentCountry.equals(lastCountry)) continue;
                        else if(!currentCountry.equals("")){
                            marker.setSelected(true);
                            lastCountry = currentCountry;
                            int id = getCountryId(currentCountry);
                            if(id != -1){
                                String currentValue = data[id][currentDomain];
                                if(!currentValue.equals("")){
                                    value += Float.parseFloat(currentValue);
                                    num += 1;
                                }
                            }
                        }
                    }

                }
            }
            meanValue = value / num;
            calcul = false;
        }

        fill(0,0,0);
        text("mean value for chosen countries is " + new DecimalFormat("0.00").format(meanValue), 400, 900);
           text("total value for chosen countries is " + new DecimalFormat("0.00").format(value), 400, 930);
    }

    void ModeCycle(){
           stroke(0);
           fill(107,174,214, 0);
           ellipse(lineX,lineY,radius * 2,radius * 2);

       if(mousePressed){
         float dis = dist(mouseX,mouseY, lineX,lineY);
            if(dis < radius - 1){
                moveC = true;
            }
            else if(dis < radius + 3 && dis > radius -1){
                changeSize = true;
            }
        }

       if(calcul == true){
         value = 0;
         num = 0;
          for (Marker marker : map.getMarkers()) {
              //println(marker.getProperty("name"));
              marker.setSelected(false);
          }

             // noFill();
            //tint(255,50);


           String lastCountry = "";
            String currentCountry = "";
            for(int i = -(int)radius;i < (int)radius;i += 5){
                for(int j = -(int)radius;j < (int)radius;j +=5){
                    float dis = dist(lineX, lineY, lineX + i, lineY + j);
                    //(""+ dis);
                    if(dis <= radius){
                        Marker marker = map.getFirstHitMarker(lineX + i, lineY + j);
                        if (marker != null) {
                            currentCountry = marker.getStringProperty("name");
                            if(currentCountry!= null){
                                if(currentCountry.equals(lastCountry)) continue;
                                else if(!currentCountry.equals("")){
                                    marker.setSelected(true);
                                    lastCountry = currentCountry;
                                    int id = getCountryId(currentCountry);
                                    if(id != -1){
                                        String currentValue = data[id][currentDomain];
                                        if(!currentValue.equals("")){
                                            value += Float.parseFloat(currentValue);
                                            num += 1;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }

           meanValue = value / num;

            calcul = false;



           //ellipse(lineX,lineY,radius * 2+1,radius * 2+1);
       }

      fill(0,0,0);
        text("mean value for chosen countries is " + new DecimalFormat("0.00").format(meanValue), 400, 900);

        text("total value for chosen countries is " + new DecimalFormat("0.00").format(value), 400, 930);
    }

    void ModeFreeChoose(){
      if(clicked){
        for (Marker marker : map.getMarkers()) {
              //println(marker.getProperty("name"));
              marker.setSelected(false);
          }
       // println("get here");
          for (Marker marker : map.getMarkers()) {
              //println(marker.getProperty("name"));
              marker.setSelected(false);
          }
          Marker marker = map.getFirstHitMarker(mouseX, mouseY);
          if (marker != null) {
             choosedCountry.add(marker.getStringProperty("name"));
             //marker.setSelected(true);
             choosedMarker.add(marker);
          }
          for(Marker m:choosedMarker){
              m.setSelected(true);
          }
          clicked = false;
          //for(String s:choosedCountry){
            //  println(choosedCountry);
          //}
      }

    }


    void ShowText(){

        fill(0,0,0);
        switch(currentMode){
            case MODE_DEFAULT:{
                text("no mode", x, y + h/2);
                break;
            }
            case MODE_HORIZONTAL:{
                text("vertical", x, y + h/2);
                break;
            }
            case MODE_VERTICAL:{
                text("horizontal", x, y + h/2);
                break;
            }
            case MODE_CYCLE:{
                text("cycle", x, y + h/2);
                break;
            }
            case MODE_FREECHOOSE:{
                text("free choose", x, y + h/2);
                break;
            }
            default: break;
        }
    }

    void dottedLine(float x1, float y1, float x2, float y2, float steps){
       for(int i=0; i<=steps; i++) {
           float x = lerp(x1, x2, i/steps);
           float y = lerp(y1, y2, i/steps);
           noStroke();
           ellipse(x, y,2,2);
     }
}
}
