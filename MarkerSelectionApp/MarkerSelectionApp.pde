/**
 * Loads country markers, and highlights a polygon when the user hovers over it.
 *
 * This example starts in Southeast Asia to demonstrate hovering multi-marker polygons
 * such as Indonesia, Phillipines, etc.
 */



import de.fhpotsdam.unfolding.*;
import de.fhpotsdam.unfolding.data.*;
import de.fhpotsdam.unfolding.geo.*;
import de.fhpotsdam.unfolding.marker.*;
import de.fhpotsdam.unfolding.utils.*;
import java.util.*;
import controlP5.*;
import processing.core.PApplet;


Base b;
PGraphics newLayer;
PFont labelFont;
import java.text.DecimalFormat;

int sizeX = 1100;
int sizeY = 1000;
UnfoldingMap map;
List countryMarkers = new ArrayList();
Location indonesiaLocation = new Location(60, 0);
chooseMode cButton;
ControlP5 controlP5;
ScrollableList selectList;
int myCurrentIndex = 0;
String attribute;
float mean = 0;

String[] columns;
String[][] data;
int dataNumber = 0;
void setup() {
  size(sizeX, sizeY, P2D);

  newLayer = createGraphics(500, 220, P3D,  getName());
  labelFont = loadFont("Courier-32.vlw");
  b = new Base(this, newLayer);
  cButton = new chooseMode();

  readData();

  map = new UnfoldingMap(this);
  map.zoomAndPanTo(indonesiaLocation, 2);
  //MapUtils.createDefaultEventDispatcher(this, map);

  List countries = GeoJSONReader.loadData(this, "countries.geo.json");

  List countryMarkers = MapUtils.createSimpleMarkers(countries);

  map.addMarkers(countryMarkers);

  //println("find a id" + getCountryId("China"));

  addSelectAttribute();
  b.loadStrings(cButton.choosedCountry, cButton.currentMode);
}

void draw() {
  background(240);

  if(MODE_DEFAULT == 0)
  //tint(255);
  map.draw();

  //choose a attribute
  textSize(18);
  text("Please choose: ",50,50);
  text("The mean value of all countries is " + new DecimalFormat("0.00").format(mean),270,50);
  CColor more = new CColor();
  more.setForeground(color(253, 141, 60));
  more.setBackground(color(253, 141, 60));
  CColor less = new CColor();
  less.setForeground(color(253, 208, 162));
  less.setBackground(color(253, 208, 162));
  controlP5.addButton("more than mean").setPosition(380,60).setSize(120,25).setColor(more);
  controlP5.addButton("less than mean").setPosition(380,100).setSize(120,25).setColor(less);


  cButton.draw();
  b.draw(newLayer, 500, 220);
  //b.draw(newLayer, 800, 800);
  //image(newLayer, 0, 0);
  image(newLayer, width/2, 70);

}

 void mouseClicked(){
     if(cButton.in == true){
        cButton.currentMode = (cButton.currentMode + 1) % 5;

     }
     cButton.clicked = true;
     for(int i=0; i<cButton.choosedCountry.size(); i++){
       System.out.println(cButton.choosedCountry.get(i));
     }
     b.loadStrings(cButton.choosedCountry, cButton.currentDomain);
 }

void mouseMoved() {
  // Deselect all marker
 /*for (Marker marker : map.getMarkers()) {
                //println(marker.getProperty("name"));
                marker.setSelected(false);
            }
*/
  // Select hit marker
  // Note: Use getHitMarkers(x, y) if you want to allow multiple selection.
  Marker marker = map.getFirstHitMarker(mouseX, mouseY);
  if (marker != null) {

  if(myCurrentIndex == 0){
      text(marker.getStringProperty("name"), mouseX, mouseY);
  }
  else {
          int id = getCountryId(marker.getStringProperty("name"));
          if(id != -1){
              String currentValue = data[id][myCurrentIndex];

              if(!currentValue.equals("")){
                  text(marker.getStringProperty("name") + " " +new DecimalFormat("0.00").format(Float.parseFloat(currentValue)), mouseX, mouseY);
                  //text(marker.getStringProperty("name") + " " +currentValue, mouseX, mouseY);
              }
          }
    }
  }


}

void mouseDragged(){
    if(cButton.locked){
        cButton.lineX = mouseX;
        cButton.lineY = mouseY;
    }
    if(cButton.moveC){

        cButton.lineX = mouseX;
        cButton.lineY = mouseY;
    }
    else if(cButton.changeSize){
        float dis = dist(cButton.lineX,cButton.lineY,mouseX, mouseY);
        if(dis < 60){
           cButton.radius = dis;
        }
    }
}

void mouseReleased(){
    cButton.locked = false;

    cButton.moveC = false;
    cButton.changeSize = false;
    //println("relseaed");
    cButton.calcul = true;
    /*if(cButton.currentMode == MODE_HORIZONTAL){
     for (Marker marker : map.getMarkers()) {
                //println(marker.getProperty("name"));
                marker.setSelected(false);
      }
      for(int i = 0;i < sizeY;i += 3){
          Marker marker = map.getFirstHitMarker(cButton.lineX, i);
          if (marker != null) {
              marker.setSelected(true);
          }
      }
    }*/

}

void readData(){
    String lines[] = loadStrings("factbook.csv");
    columns = lines[0].split(";");
    data = new String[lines.length - 2][];
    dataNumber = lines.length - 2;
    for(int i = 0;i < lines.length -2;i ++){
       data[i] = lines[i + 2].split(";");
    }
   // for(int i = 0;i < lines.length - 2;i ++)
     //   println(data[i][0]);
    //println("Loaded " + lines.length + " lines");
    //println(lines[2]);
}

int getCountryId(String name){
    for(int i = 0;i < dataNumber;i ++){
        if(data[i][0].equals(name))
            return i;
    }
    return -1;
}

void addSelectAttribute(){
    //selection box
  controlP5 = new ControlP5(this);
  selectList = controlP5.addScrollableList("attribute",50,60,200,200);
  selectList.setItemHeight(30);
  selectList.setBarHeight(30);

  selectList.setColorBackground(color(158,154,200));
  //selectList.setColorBackground(color(255,128));
  //selectList.setColorActive(color(255,0,0,128));
  selectList.setColorLabel(color(0,0,255));
  //selectList.setColorForeground(color(255,0,0));
  //selectList.setColor(color(255,0,0));
  for(int i = 0; i<columns.length; i++){
    selectList.addItem(columns[i],i);
  }
}

void controlEvent(ControlEvent theEvent) {

  if (theEvent.isFrom(selectList))
  {
     myCurrentIndex = (int)theEvent.getValue();
     cButton.currentDomain = myCurrentIndex;
     attribute = columns[myCurrentIndex];
     println(attribute);
  }

  colorCounties();

}

void colorCounties(){

  float total = 0;
  //min, max, mean;
  int num = 0;

      for(int i = 0;i < dataNumber;i ++){
        if(data[i].length <= myCurrentIndex)  continue;
          String currentValue = data[i][myCurrentIndex];
          if(!currentValue.equals("")){
            total += Float.parseFloat(currentValue);
            num++;
          }
      }
      mean = total/num;

      for (Marker marker : map.getMarkers()) {
           marker.setSelected(false);
             if (marker != null) {
                 String currentCountry = marker.getStringProperty("name");
                 if(currentCountry!= null){

                    for(int j=0; j<dataNumber; j++){
                      String countryName = data[j][0];
                      if(currentCountry.equals(countryName)){

                        //find the current country attribute value and judge it
                        if(data[j].length <= myCurrentIndex) continue;
                        String currentValue = data[j][myCurrentIndex];
                        float value = 0;
                        if(!currentValue.equals("")){
                            value = Float.parseFloat(currentValue);
                        }
                        //marker.setSelected(true);
                        if(value>=mean){
                          marker.setColor(color(253, 141, 60));
                        }else{
                          marker.setColor(color(253, 208, 162));
                        }
                        break;
                      }
                    }

                  }
             }
      }


}
